import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { Actions } from 'react-native-router-flux';
export default class First extends Component {
  render() {
    return (
      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
        <Text onPress={()=>Actions.second()}> First Screen </Text>
      </View>
    )
  }
}
