import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { Router, Scene, drawer, Actions } from 'react-native-router-flux'
import First from './First';
import Second from './Second';
import Menu from './Menu';
import { Button } from 'native-base';
// import { Icon } from 'react-native-vector-icons/Ionicons';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    // const menuIcon = () => {
    //   return (
    //     <Icon name='menu' color='red' style={{ heigh: 30, width: 30 }} />
    //   )
    // }
    const myImage = require('./img/icon.png');
    return (
      <Router>
        <Scene key='root'>
          <drawer
            hideNavBar
            key='drawer'
            drawer
            contentComponent={Menu}
            // drawerIcon={menuIcon}
            // drawerImage={myImage}
            drawerWidth={300}
            initial
            renderLeftButton={() => 
            <Button transparent onPress={()=>Actions.drawerOpen()}>
              <Image source={require('./img/icon.png')} style={{ width: 40, height: 25,marginLeft:10}} />
            </Button>}

          >
            <Scene
              key='first'

              component={First}
            />
            <Scene
              key='second'
              title='Screen Two'
              component={Second}
            />
          </drawer>
        </Scene>
      </Router>

    );
  }
}
