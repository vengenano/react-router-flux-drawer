import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { Actions } from 'react-native-router-flux';
export default class Second extends Component {
  render() {
    return (
      <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
        <Text onPress={()=>Actions.first()}> Second Screen </Text>
      </View>
    )
  }
}
