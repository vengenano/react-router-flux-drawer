import React, { Component } from 'react'
import { Text, View, Image } from 'react-native'
import { Content, List, ListItem } from "native-base"
export default class Menu extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('./img/logo-KD-small.png')} style={{ width: 100, height: 100 }} />
                    <Text style={{ fontSize:20,fontWeight:'bold',marginTop:12}} >Name : Sorn Veng e</Text>
                </View>
                <View style={{ flex: 2 }}>
                    <Content>
                        <List>
                            <ListItem>
                                <Text>User Profile</Text>
                            </ListItem>
                            <ListItem>
                                <Text>Notification</Text>
                            </ListItem>
                            <ListItem>
                                <Text>Polocy</Text>
                            </ListItem>
                            <ListItem>
                                <Text>About us</Text>
                            </ListItem>
                            <ListItem>
                                <Text>Setting</Text>
                            </ListItem>
                        </List>
                    </Content>
                </View>
            </View>
        )
    }
}
